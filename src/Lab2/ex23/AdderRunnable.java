package Lab2.ex23;

import java.util.ArrayList;

public class AdderRunnable implements Runnable {
    private ArrayList<Double> list;
    private Double sum;

    public AdderRunnable(ArrayList<Double> list) {
        this.list = list;
        this.sum = 0.0;
    }

    @Override
    public void run() {
        long start = System.currentTimeMillis();
        this.list.forEach((num) -> {
            this.sum += num;
        });
        System.out.println("Ejecución terminada con tiempo: " + (System.currentTimeMillis() - start));
    }

    public Double getSum() {
        return sum;
    }
}
