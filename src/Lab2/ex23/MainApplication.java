package Lab2.ex23;

import java.util.ArrayList;
import java.util.Scanner;

public class MainApplication {
    public static void main(String[] args) {
        ArrayList<Double> list = new ArrayList<>();
        ArrayList<Thread> threads = new ArrayList<>();
        ArrayList<AdderRunnable> adders = new ArrayList<>();
        Double totalSum = 0.0;

        Scanner sc = new Scanner(System.in);

        System.out.print("Number of threads: ");
        int p = sc.nextInt();
        System.out.print("Size of numbers array: ");
        int n = sc.nextInt();

        for (int i = 0; i < n; i++) {
            //list.add((double)(Math.random() * 50 +1));
            list.add((double) i+1);
        }

        int chunkSize = (int) ((double) Math.ceil(list.size() / (double)p));

        for (int i = 0; i < p; i++) {
            int counter = 0;
            boolean flag = true;

            ArrayList<Double> sublist = new ArrayList<>();

            while(counter < chunkSize && flag) {
                int index = chunkSize * i + counter;
                if (index < n) sublist.add(list.get(index));
                else flag = false;
                counter++;
            }
            AdderRunnable adder = new AdderRunnable(sublist);
            adders.add(adder);
            threads.add(new Thread(adder));
        }

        threads.forEach(thread -> thread.start());
        threads.forEach(thread ->  {
            try {
                thread.join();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        });

        for (AdderRunnable adder: adders) {
            totalSum += adder.getSum();
        }
        System.out.println(totalSum);
    }
}
