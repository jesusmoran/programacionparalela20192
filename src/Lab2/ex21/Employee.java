package Lab2.ex21;

public class Employee extends Thread {
    private String name;

    public Employee (String name) {
        this.name = name;
    }

    @Override
    public void run() {
        try {
            this.wash();
            this.extract();
            this.wax();
        } catch (Exception e) {
            System.out.println("Exception handled: " + e.getMessage());
        }
    }

    public void wash() throws InterruptedException {
        System.out.println("Employee " + name + " washing car");
        Thread.sleep(this.randomTime());
    }

    public void extract() throws InterruptedException {
        System.out.println("Employee " + name + " extracting car");
        Thread.sleep(this.randomTime());
    }

    public void wax() throws InterruptedException {
        System.out.println("Employee " + name + " waxing car");
        Thread.sleep(this.randomTime());
    }

    private int randomTime() {
        return (int)((Math.random() * 10 +1) * 1000);
    }
}
