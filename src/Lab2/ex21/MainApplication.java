package Lab2.ex21;

public class MainApplication {
    public static void main(String[] args) {
        Employee employee1, employee2;

        employee1 = new Employee("Carlos");
        employee2 = new Employee("Pepe");

        employee1.start();
        employee2.start();
    }
}
