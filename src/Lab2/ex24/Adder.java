package Lab2.ex24;

import java.util.ArrayList;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

public class Adder extends RecursiveTask<Double> {
    private ArrayList<Double> list;
    private int chunkSize;
    private int end, start;

    public Adder(ArrayList<Double> list, int start, int end, int chunkSize) {
        this.list = list;
        this.start = start;
        this.end = end;
        this.chunkSize = chunkSize;
    }

    private Adder(ArrayList<Double> list, int chunkSize) {
        this(list, 0, list.size(), chunkSize);
    }

    @Override
    protected Double compute() {
        int length = this.end - this.start;
        if (length <= chunkSize) {
            return sumAction();
        }
        Adder firstTask = new Adder(list, start, start + length / 2, chunkSize);
        firstTask.fork();

        Adder secondTask = new Adder(list, start + length / 2, end, chunkSize);
        Double result2 = secondTask.compute();
        Double result1 = firstTask.join();

        return result1 + result2;
    }

    public static Double startComputing(ArrayList<Double> list, int chunkSize) {
        ForkJoinTask<Double> task = new Adder(list, chunkSize);
        return new ForkJoinPool().invoke(task);
    }

    public Double sumAction() {
        Double sum = 0.0;
        for (int i = this.start; i < this.end; i ++) {
            sum += list.get(i);
        }
        return sum;
    }
}
