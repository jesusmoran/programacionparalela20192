package Lab2.ex24;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ForkJoinPool;

public class MainApplication {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        ArrayList<Double> list = new ArrayList<>();

        System.out.print("Number of threads: ");
        int p = sc.nextInt();
        System.out.print("Size of numbers array: ");
        int n = sc.nextInt();

        int chunkSize = (int) ((double) Math.ceil((double) n / (double)p));

        for (int i = 0; i < n; i++) {
            //list.add((double)(Math.random() * 50 +1));
            list.add((double) i+1);
        }

        System.out.println(Adder.startComputing(list, chunkSize));
    }
}
