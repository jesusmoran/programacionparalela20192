import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainApplication {
    public static void main(String []args) throws InterruptedException {

        ArrayList<Cliente> clientes = new ArrayList<Cliente>();

        //Seed clientes
        for (int i = 0; i < 7; i++) {
            clientes.add(new Cliente("Cliente " + i));
        }

        ExecutorService executor = Executors.newFixedThreadPool(3);

        for (Cliente c : clientes) {
            CajeraRunnable cajera = new CajeraRunnable(c);
            executor.execute(cajera);
        }
        executor.shutdown();
        while (!executor.isTerminated()) {}
    }
}
