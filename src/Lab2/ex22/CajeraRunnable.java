import java.util.ArrayList;

public class CajeraRunnable implements Runnable {

    private Cliente clienteActual;

    public CajeraRunnable(Cliente c) {
        this.clienteActual = c;
    }

    @Override
    public void run() {
        if (clienteActual != null) {
            ArrayList<Item> items = clienteActual.getItems();
            items.forEach(item -> this.procesarProducto(item));
        }
    }

    private void procesarProducto(Item i) {
        try {
            System.out.println("Tiempo de procesado: " + i.getTiempoProcesado() + " segundos. En thread " + Thread.currentThread().getName());
            Thread.sleep(i.getTiempoProcesado() * 1000);
            System.out.println("Producto " + i.getNombre() + " procesado correctamente. En thread " + Thread.currentThread().getName());
        } catch (Exception e) {
            System.out.println("Error procesando producto: " + e.getMessage());
        }
    }

}
