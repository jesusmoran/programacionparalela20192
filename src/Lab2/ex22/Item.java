public class Item {
    private String nombre;
    private int tiempoProcesado;

    public Item(String nombre, int tiempoProcesado) {
        this.nombre = nombre;
        this.tiempoProcesado = tiempoProcesado;
    }

    public String getNombre() {
            return nombre;
    }

    public int getTiempoProcesado() {
        return tiempoProcesado;
    }
}
