import java.util.ArrayList;

public class Cliente {
    private String nombre;
    private ArrayList<Item> items;

    public Cliente(String nombre) {
        this.nombre = nombre;

        this.items = new ArrayList<Item>();
        for (int i = 0; i < 20; i++) {
            items.add(new Item("Item " + i, (int) (Math.random() * 10 + 1)));
        }
    }

    public String getNombre() {
        return nombre;
    }

    public ArrayList<Item> getItems() {
        return items;
    }
}
